---
layout: markdown_page
title: "Category Direction - Credential Management"
description: Lifecycle management of credentials, including tokens, for both users and group owners/admins
canonical_path: "/direction/manage/auth/credential-management/"
---

- TOC
{:toc}

## Permissions

| **Stage** | **Maturity** | **Content Last Reviewed** |
| --- | --- | --- |
| [Manage](/direction/dev/#manage) | [Minimal](/direction/maturity/) | `2022-05-26` |

### Overview

Credential Inventory for GitLab.com (e.g. SSH and PAT expiration) provide administrators with visibility and control for the credentials that can access their instance or group(s). Today much of this functionality is only available to self-managed instances.

#### Target Audience





#### What’s Next & Why

We will bring the credential inventory to GitLab.com to allow managing the credentials that are in use in different groups and projects. This will help group owners to meet compliance requirements and minimize the chance of any credentials being used inappropriately.

#### What is Not Planned Right Now



#### Maturity


### User Success Metrics

What success looks like: GitLab provides a built-in experience that can be used to monitor, revoke, and ensure rotation of the credentials used to access GitLab. We currently have the Credential Inventory to provide some of this information on self-managed GitLab and will continue to enhance it to provide more information and to work for customers on SaaS.

### Competitive landscape


### Top Vision issue(s)


## How you can help




