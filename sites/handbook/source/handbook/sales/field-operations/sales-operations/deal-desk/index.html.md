---
layout: handbook-page-toc
title: "Deal Desk Handbook"
description: "The Deal Desk team's mission is to streamline the opportunity management process while acting as a trusted business partner for field sales."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

## **Welcome to The Deal Desk Handbook** 

The Deal Desk team's mission is to streamline the opportunity management process while acting as a trusted business partner for field sales. We are the first point of contact for sales support. 

### Helpful Links

*   **Salesforce Reports and Dashboards**
    *   [Current Quarter WW Sales Dashboard](https://gitlab.my.salesforce.com/01Z4M0000007H7W)
    *   [Monthly Bookings Report](https://gitlab.my.salesforce.com/00O61000004Ik27)
    *   [Deal Desk Pending Opportunity Approvals Report](https://gitlab.my.salesforce.com/00O4M000004e0Dp)

*   **Frequently Used Handbook Pages**
    *   [Sales Order Processing](/handbook/business-ops/order-processing/)
    *   [Deal Desk Opportunity Approval Process](/handbook/business-ops/order-processing/#submitting-an-opportunity-for-deal-desk-approval)
    *   [Useful Company Information](https://gitlab.com/gitlab-com/finance/wikis/company-information)
    *   [Account Ownership Rules of Engagement](/handbook/sales/field-operations/gtm-resources/rules-of-engagement/#account-ownership-rules-of-engagement)
    *   [ARR in Practice](https://about.gitlab.com/handbook/sales/sales-term-glossary/arr-in-practice/)
    *   [Vendor Setup Form Process](/handbook/business-ops/order-processing/#how-to-process-customer-requested-vendor-setup-forms)
    *   [Security Questionnaire Process](/handbook/engineering/security/#process)
    *   [Troubleshooting: True Ups, Licenses + EULAS](/handbook/business-ops/business_systems/portal/troubleshooting/)
    *   [Licensing FAQ](https://about.gitlab.com/pricing/licensing-faq/)
    *   [Legal Authorization Matrix](/handbook/finance/authorization-matrix/)
    *   [Trade Compliance (Export/Import)](/handbook/business-ops/order-processing/#trade-compliance-export--import-and-visual-compliance-tool-in-salesforce)

*   **Other Resources**
    *   [Quote Approval Matrix](https://docs.google.com/document/d/1-CH-uH_zr0qaVaV1QbmVZ1rF669DsaUeq9w-q1QiKPE/edit?ts=5d6ea430#heading=h.ag75fqu12pf0)
    *   [Billing FAQs and Useful Tips](https://gitlab.com/gitlab-com/finance/-/wikis/Billing-Team-FAQs-&-Useful-Tips)
    *   Sample Order Form 

### What We Do
    
*  Sales Support 
*  Troubleshooting Quote Configuration
*  Complex/Non-standard Deal Structure
*  Deal Structure Approval Guidance
*  Month End / Quarter End Reconciliation 

#### Out of Scope

* Standard Quote Creation
* Opportunity Creation
* Forecast Management 
* RFP/Vendor Form Responses
* Customer / Partner facing communication 

### Who and Where We Are

#### Regional Support

The Deal Desk team is located around the world and will be available during standard business hours within most regions. We operate under a Regional Support Model, meaning, each region (EMEA/APAC/AMER) will receive support from a dedicated regional team of Deal Desk Specialists and Analysts. 

For Alliances & Private Offer Creation, this process is supported by one team member globally, with managers trained as back ups for escalation points. For all Alliances deals, each region will be supported by a dedicated Deal Desk Analyst who specializes in Alliances, based out of AMER (Pacific Time). 

During holidays, or when team members are on PTO, individuals located in a different regional may step in to support and ensure appropriate coverage. This is reserved for End of Month/End of Quarter, or other times when the team is short-staffed.

Support will be provided based on the following business hours in the respective region. If a complex case or non standard deal request is submitted after 4:00PM (Local Time) the case will be prioritized the following business day.  

|     Region    | Standard Support Hours |
|:-------------:|------------------------|
| APAC          | 8:00AM to 5:00PM (IST)   |
| EMEA          | 8:00AM to 5:00PM (GMT)   |
| AMER / LATAM  | 7:00AM to 5:00PM (PT)    |

Our regional teams do not operate on a 24/7 support model. If your request is submitted after 4:30PM local time, or outside of standard support hours, it may not be reviewed until the following business day. The only exceptions for this are End of Month/End of Quarter. Support hours and availability will be shared in advance of high volume periods through #field-fyi. 

#### Regional Structure

As of 2022-02-01, the Deal Desk team is structured as follows:

**AMER Team:**
- Manager, Deal Desk (AMER)
  - Deal Desk Analyst, Alliances (Global)
  - Deal Desk Specialist (AMER)
  - Deal Desk Specialist (AMER)
  - Deal Desk Specialist (AMER)
  

**EMEA/APAC TEAM:**
- Manager, Deal Desk (EMEA/APAC)
  - Deal Desk Analyst (APAC)
  - Deal Desk Analyst (EMEA)
  - Deal Desk Specialist (EMEA)
  - OPEN REQ: Deal Desk Specialist (EMEA)

### Communicating with the Deal Desk Team

#### Salesforce Chatter Communication

Deal Desk's primary communication channel is Salesforce Chatter. When you chatter `@Sales-Support`, it will automatically create a case in the Deal Desk/Order Management queue. 
- Always use `@Sales-Support` for SFDC requests or post `#sales-support` in Slack for general questions.
  - Please do not tag Deal Desk team members directly in chatter or make a request through Slack direct message. This ensures our team is working as efficiently as possible and that you are covered in case the DD team member who replied first is unavailable. 
  - Direct chatters to team members will not create a case in the Sales Support case queue, and therefore will not be prioritized. You may experience a significant delay in response, or no response at all. Please always use `@Sales-Support` to communicate with the team in chatter. 
- If someone is working on a case, they will continue to support until the case is closed.  If an issue has been resolved, please chatter @Sales-Support to reopen a case.
- If you tag @Sales-Support on an existing Chatter post/comment by editing it, that action won`t generate a request to Deal Desk and we will not receive a notification. Please make sure tag Sales Support on an original post or on a new comment to the original post.

For cases related to Account Segmentation (ROE, Territory), account data, or account merges, Deal desk will reassign these cases to our Sales Operations team to review and resolve. 

#### Slack Communication

##### Primary Slack Channel

Use our Slack channel in case of general, non-record related requests and/or urgent questions: 
**#sales-support** [If the request is related to a quote, opportunity, or account - please chatter on the page in Salesforce instead of the Slack channel.] 

##### Slack Best Practices

**Please avoid contacting the DD team members directly via Slack.** Utlizing the `#sales-support` channel is best to ensure timely coverage, helps others who may have similar questions, and aligns with our **Transparency** value. 

In case of a specific opportunity or quote related question please use SF Chatter (see section [Salesforce Chatter Communication](#salesforce-chatter-communication))

##### Slack Announcements

Desk Desk process updates and announcements will be communicated via #field-fyi, #sales and #sales-support Slack channels.  

#### Deal Desk Office Hours

Weekly Deal Desk Office Hours are scheduled as follows:
- EMEA Office Hours: Thursday at 10:30 GMT,
- US Office Hours: Wednesday at 12:00 PM EST. 

During Month End, Office Hours will take place on Monday, Wednesday, and Friday, scheduled in both AMER and EMEA time zones. Calendar invites will be sent to Sales-All Distribution group. Priority will be given to opportunities closing within the quarter. 

Supported topics include:
* Create or modifying a quote
* Quote approval acceleration
* Net ARR calculation
* Submitting an opportunity for close
* Validation/segmentation of closed opportunities
* And anything else to help drive opportunities closing within the quarter! 

#### Deal Desk AMA

Deal Desk AMA's are an opportunity to invite someone from Deal Desk to your local team call. We can help address any questions regarding quote processes, best practices, or unique deal structures. Think of this as an opportunity for your team to learn together on general topics for quoting or opportunity management. For specific questions related to in-flight opportunities, it is best to join [Deal Desk Office Hours](/handbook/sales/field-operations/sales-operations/deal-desk/#deal-desk-office-hours). 

Chatter or Slack #sales-support if you are interested in having Deal Desk join your team call! 

### Key Performance Indicators

#### 1. Deal Desk  Case SLAs 

The Deal Desk team will do their best to respond to each request to '@sales-support' within 6 business hours. Revenue generating or current quarter requests will take priority, especially during Month & Quarter End. If a task is not resolved within 24 hours it will be escalated (if necessary). 
The SLAs below are applicable (especially for contract resets and ramp deals) if all the necessary information is provided in the request.

| Type of Request | First Response | Resolution |
|----- | ----- | ------| 
| Basic Quote Assistance | 6 Hours | 8 Hours | 
| Ramp Deal | 6 Hours | 24 hours |
| Alliances, General Support | 8 Hours | 48 Hours | 
| Alliances, Private Offer Creation | 24 Hours | 48 Hours | 
| Flat Renewal | 6 Hours | 24 Hours |
| IACV/ARR Review | 6 Hours | 24 Hours |
| Contract Reset / Co-Term | 6 Hours | 24 Hours | 
| RFP/Vendor Forms | 6 Hours | Dependent on AM |

##### SLA for EoA Ramp Deal Requests

The Deal Desk team will respond to E0A Ramp requests as quickly as possible. However, due to the complexity involved in building ramp deals, the SLA for an EoA Ramp Deal Order Form creation is as follows:

| Subscription Term Start Date | First Response | Resolution |
|----- | ----- | ------| 
| < 30 days in the future | 6 Hours | 1 Business Day | 
| > 30 days in the future| 6 Hours | 3 Business Days |

A Deal Desk team member will respond to your chatter request with an estimate on when the Order Form will be complete. Please let us know if your customer requires the quote in advance due to lengthy procurement cycles or budget planning reasons - exceptions will be considered in cases of urgency, or to meet a customer's needs.

##### Escalation Path for Case Review

Cases that are urgent, customer impacting, or critical to business may be prioritized upon request. If the case has been submitted after Standard Support hours, you may escalated the case for a different regional team to review in the #sales-support Slack channel. You may also tag regional Deal Desk Managers for review and assistance in delegating the case.

Cases in this queue must be urgent and business critical. Cases that are routinely escalated out of individual convenience instead of legitimate urgency will be addressed with Sales Management.

##### Reports To Track Deal Desk SLA 

[Cases / Average First Response](https://gitlab.my.salesforce.com/00O4M000004edoT)

#### 2. Regional Support Satisfaction 

All regions will receive a Quarterly Customer Satisfaction survey at the beginning of each new quarter. Our team aims for a 92% satisfaction rating for their supported region. This survey is a valuable tool to provide feedback on areas of improvement related to the Quote to Cash lifecyle.

Results from the survey will be used to evaluate team member performance and to identify areas for improvement.

**Measuring Regional Support Satisfaction:**
* A positive satisfaction rating is determined by measuring the ratio of positive responses to the question: "How would you describe the level of support you received from Deal Desk during Q1?" 
* Available responses include:
  * Excellent
  * Good
  * Neutral
  * Poor
  * Very Poor
* Satisfaction rating is determined by the percentage of Excellent, Good, and Neutral responses, as compared to the total number of responses. The outcome in percentage form is the Regional Support Satisfaction rating. 

#### 3. Case Reduction through Proactive Support - Training, Documentation, and System Improvements 

The Deal Desk team will be responsible for taking action on feedback received in our Quarterly CSAT survey by improving process related to common quoting errors, opportunity bugs, or gaps in process documentation. The goal is to decrease the number of cases and enable the field to complete the standard duties related to quoting without Deal Desk intervention. Deal Desk will always assist in complex or non-standard deals.

### Key Performance Indicators: Results

To review quarterly KPI results, see: [Deal Desk & Order Management KPI - Results](https://about.gitlab.com/handbook/sales/field-operations/sales-operations/Deal-Desk-Order-Mgmt-KPIs/)

### Proactive Opportunity Review

In an effort to proactively prevent common order issues (especially those that would require a customer to resign our order form) the Deal Desk team will conduct a manual review of all opps that meet the following criteria: 

- Close Date = Within Current Quarter
- Net ARR = $100k+ 
- Opportunity Stage - 4 OR 5 

Team members will review opps in their region each week. We are looking for anything that would prevent the opportunity from booking smoothly, including:

1. Check that quote is built correctly for the opportunity type and route to market (direct, channel, marketplace)
2. Review quote approval requirements are met
3. Review IACV / ARR (IACV review through FY21Q1)
4. Review PO Requirement. If the account requires a PO, chatter the rep with a reminder
5. Check with rep to determine whether any questions or additional needs, especially if you see something structured oddly on the opp/quote, or if it is a complex deal

**Review Cadence** 

The team will review opps on a monthly basis, as time allows throughout the work week. During the last month of any quarter, the review will be conducted weekly. 

### Calculating ARR

To calculate ARR, please review the [ARR in Practice](https://about.gitlab.com/handbook/sales/sales-term-glossary/arr-in-practice/#annual-recurring-revenue-arr-and-salesforce) handbook page.

### **Zuora Quote Configuration Guide - Standard Quotes**

The following is intended as a step by step guide for opportunity owners or ISR's to learn how to create standard quotes for New Subscriptions, Amendments, and Renewal opportunities. 

#### New Subscription Quote

Follow this step by step guide for creating a **New Subscription** quote. Use a New Subscription quote when the customer is purchasing a brand new subscription, OR if the deal structure includes a Contract Reset. 

A.  Open the New Business opportunity and click the **“New Quote”** button.

B.  When prompted **select “New Subscription”** and click “Next.”

C.  Provide Quote, Account, and Subscription Term Details and click “Next.”

*   Select a Quote Template

| Template                            | Use For                                                                                                  |
|-------------------------------------|----------------------------------------------------------------------------------------------------------|
| Standard Order Form                 | Most quotes, including alliance marketplace transactions, EDU/OSS/YC, or Customers with an Existing Agreement (MSA) in place |
| Standard Order Form (Hide Discount) | Hide the Discount Column for Direct Deals. Otherwise Identical to the Standard Order Form Template       |
| Authorized Reseller Order Form      | Authorized Reseller Transactions                                                                         |
| MSP Order Form                      | Managed Service Provider Transactions                                                                    |
| Distributor Order Form              | Distributor Transactions                                                                                 |

*   If there is a **signed MSA** between the customer and GitLab, the **“Existing GitLab Agreement Effective Date” field** should be populated with the date of the MSA’s signature. If "Existing Agreement Effective Date" is populated on the quote object, the Order form will automatically generate with Acceptance Language that References the existing agreement. If it **is not populated** the language will default to Standard terms. 

*   Add any preapproved legal language selections to the order form. Checking the box next ot the language selection will automatically generate an order form with pre approved legal language - no need for Deal Desk intervention. Selections are as follows: 

| Toggle Field                   | Output                                                                                   |
|--------------------------------|------------------------------------------------------------------------------------------|
| Annual Payments                | Annual Payment Language will populate in Payment Details on the Order Form PDF           |
| Customer Reference Language    | Customer Reference Language will populate in Notes Section of the order form             |
| Add Quarterly True Up Language | Standard Quarterly True Up language will populate in the Notes section of the Order Form |
| Remove Signature Block         | Signature Block will be removed. Use for customers with Existing Agreements (MSA)        |

*   **Select “Sold To” and “Bill To” contacts.** Note that the “Sold To” contact will receive the EULA or License file via email. Note: Each contact record must have a complete address, if the address is not fully populated, you will need to update this before you can proceed with the quote. 
*   For **Channel deals,** populate the “Invoice Owner”, "Invoice Owner Contact" and "Resale Partner" fields. For **Direct Deals** leave "Invoice Owner", "Invoice Owner Contact" and "Resale Partner" blank. 
    *   Ensure that the Invoice Owner Contact is connected to the correct reseller account - i.e. if the reseller is in Germany, the Invoice Owner Contact should also be based in Germany. The Billing Account on the Partner Account lists the contact associated with that account, so that contact should be used as the Invoice Owner Contact. If that contact does not exist on the Partner Account as a Contact Record yet, you may need to create it.
    *   If an "Invoice Owner" does not auto-populate in the drop down, this means that a Billing Account does not yet exist for the partner. Chatter `@Billing-ops` and the Partner Account Owner listed on the Partner Account Record and ask for a Billing Account to be created. 
    *   The "Resale Partner" field should be populated with the Partner Account of the Reseller transacting (please note a partner may have both a Customer and Partner Account Record, the Partner Account must be selected. Check the “Account Record Type” field on an account to identify whether it is Customer or Partner account.).

*   Populate **“Initial Term”** in months. (i.e. for a two-year deal, enter “24”). For standard deals, "Initial Term" should always match the SKU that you are quoting (i.e. Premium - 2 Year = Initial Term 24 Months). 
*   If the customer or reseller is based in the EU, enter the “VAT ID” number. This is required to book the order. 

*   Note: Non-standard payment terms (Net 30 = standard) should be reflected on the quote object, and will require approval per the approval matrix.

D.  Select **“Add Base Products”** from the drop-down menu on the “Edit Products and Charges” page. Select the correct product and click Next.

E.  Enter the product quantity, and adjust the discount or effective price as needed. Click Submit.
*   For **Channel deals**, see this [cheat sheet](https://docs.google.com/document/d/1qiT_2EsnL20c4w0hyZ_CGaJQIzj8CSCsHERoR80cwws/edit#heading=h.9e3o7yaxw8mu) to confirm correct discounts to apply. Partner discounts must be added in addition to any customer discounts and required approvals must be received by submitting the quote in the system, per the quote approval matrix. If there are no customer discounts and standard partner program discounts are used, additional quote approvals will not be required. Chatter `@Partner Operations` for questions.

F.  **Add Annual Payments** to a order form by:
* Checking "Annual Payments" checkbox on Quote Detail Page
* Updating "Initial Term" to 24, 36, 48, or 60. **If Intital Term is not updated correctly, the quote will not generate the correct Total**"
* **All Annual Payment quotes** must use the 1 - Year Product SKU. You will see an error message if you select Annual Payments and anything other than the 1 - Year SKU. 

G. SuperSonics: Review the Zuora fields to determine whether any Cloud Licensing features apply to the subscription. For more information on Cloud Licensing, see: [SuperSonics and Sales Assisted Transactions](https://about.gitlab.com/handbook/sales/field-operations/order-processing/#supersonics-and-sales-assisted-transactions) 
H.  **Order Form Generation**
*   If no discounts or special terms are requested, click “Generate PDF.”
*   If the deal contains discounts that require approval, please submit the quote for approval using the button on the quote. The quote must be approved before the PDF can be generated.
    *   In the case of reseller deals, please obtain the approvals in Chatter based on the approval matrix.
*   **Order Form Manual Edits**
    *   Please note that Sales reps may only generate PDF versions of the Order Form. If you require special wording or other manual edits, please make the request by tagging `@Sales-Support` in chatter. 

**To supplement these steps, review the [THIS TRAINING VIDEO](https://youtu.be/XqQC65-vJws) to see each step live for a New Subscription quote.**

**Note:** If you cannot view the video, make sure you are logged in to GitLab Unfiltered. You can [learn more about how to log in or request access to GitLab Unfiltered here!](/handbook/marketing/marketing-operations/youtube/#unable-to-view-a-video-on-youtube) 


#### Amend Subscription Quote


This quote type should be only used when **new users are being added to an existing subscription during the current subscription term.** This includes both additional licenses to existing products, and true-ups. This also includes any scenario where the products are being changed during the term - i.e., an upgrade from Premium to Ultimate. Amendment quotes cannot extend a subscription term. 

Amendments to subscriptions must be processed entirely before any changes can be made via additional amendments or a renewal opportunity. This means that the Add On Opportunity should be Closed-Won before submitting a Renewal opportunity for approval.

#### **A.  Add-On Quote Creation**

*   Create the Add-on opportunity on the latest closed won annual opp (Renewal or New Business) that is related to that subscription (use the "New Add On Opportunity" button)
*   Open the Add-On opportunity and click the **“New Quote”** button.
*   Select the **existing billing account**.
*   When prompted **select “Amend Existing Subscription for this Billing account**, and select the appropriate subscription. 
    *   Note: If there are multiple billing accounts or subscriptions available, and you are unable to determine which to choose, please ask @Sales-Support via Chatter.
*   Provide Quote Template, Account, and Subscription Term Details and click “Next.”

* Select a Quote Template - Note, Amendments must transact through the same route to market as the original order. 

|Template                             | Use For                                                                                                  |
|-------------------------------------|----------------------------------------------------------------------------------------------------------|
| Standard Order Form                 | Most quotes, including alliance marketplace transactions, or Customers with an Existing Agreement (MSA) in place |
| Standard Order Form (Hide Discount) | Hide the Discount Column for Direct Deals                                                                |
| Authorized Reseller Order Form      | Authorized Reseller Transactions                                                                         |
| MSP Order Form                      | Managed Service Provider Transactions                                                                    |
| Distributor Order Form              | Distributor Transactions                                                                                 |

*   If there is a **signed MSA** between the customer and GitLab, the **“Existing GitLab Agreement Effective Date” field** should be populated with the date of the MSA’s signature. If "Existing Agreement Effective Date" is populated on the quote object, the Order form will automatically generate with Acceptance Language that References the existing agreement. If it **is not populated** the language will default to Standard terms. 

*   Add preapproved legal language selections to the order form. Checking the box next ot the language selection will automatically generate an order form with pre approved legal language - no need for Deal Desk intervention. Selections are as follows: 

| Toggle Field                   | Output                                                                                   |
|--------------------------------|------------------------------------------------------------------------------------------|
| Customer Reference Language    | Customer Reference Language will populate in Notes Section of the order form             |
| Remove Signature Block         | Signature Block will be removed. Use for customers with Existing Agreements (MSA)        |
    
*   The **Start Date** cannot be set before the subscription start date.
    *   The **End Date** will automatically be set to co-terminate with the existing subscription.
    *   The **Initial Term** is automatically updated to reflect the initial term of the original New Business or Renewal subscription which is being amended by this quote.**Please do not override the automatically populated Initial Term on Amend Subscription quote objects**  (i.e. if you’re amending the quote halfway through a 12 month term, the Initial Term should be 12, not 6.)
    *   The **GitLab Entity** must be the same as it was on the initial deal you’re amending.
    *   For **Channel deals,** populate the “Invoice Owner”, “Invoice Owner Contact” and "Resale Partner" fields. For **Direct Deals** leave "Invoice Owner", "Invoice Owner Contact" and "Resale Partner" blank. 
        *   Ensure that the Invoice Owner Contact is connected to the correct reseller account - i.e. if the reseller is in Germany, the Invoice Owner Contact should also be based in Germany. The Billing Account on the Partner Account lists the contact associated with that account, so that contact should be used as the Invoice Owner Contact. If that contact does not exist on the Partner Account as a Contact Record yet, you may need to create it.
        *   If an "Invoice Owner" does not auto-populate in the drop down, this means that a Billing Account does not yet exist for the partner. Chatter `@Billing-ops` and the Partner Account Owner listed on the Partner Account Record and ask for a Billing Account to be created. 
        *   The "Resale Partner" field should be populated with the Partner Account of the Reseller transacting (please note a partner may have both a Customer and Partner Account Record, the Partner Account must be selected. Check the “Account Record Type” field on an account to identify whether it is Customer or Partner account.).
        *   **We cannot change route to market through an Amendment quote**. If the original order was sold Direct, all subsequent amendments must also be Direct. If it was initially sold through a Partner, all amendments must go through the same partner for the duration of the subscription. 
    *   If the customer or reseller is based in the EU, enter the “VAT ID” number. This is required to book the order.
    *   If there is a **signed MSA** between the customer and GitLab, the **“Existing GitLab Agreement Effective Date”** field should be populated with the date of the MSA’s signature. In addition, select the **“EXISTING MSA”** quote template.
    *   Note: Non-standard payment terms (Net 30 = standard) should be reflected on the quote object, and will require approval per the approval matrix.
*   Under the **Edit Products and Charges** page, increase the existing license quantity to reach your total - i.e. enter the new total license count. This page will show all currently licensed products (marked “Original”), but the Order Form that generates will only show the added quantity and amount for the pro-rated period. 
*   **To add users to the existing subscription at a different price or discount**, the new user licenses should be added as a separate product line. 
*   **True-Ups:** NOTE: The true-up SKU does not add users to the license - it's simply a retroactive one time charge. If you are conducting a quarterly or semi annual true-up for a customer with our former percentage-based fee schedule, do not use the true-up SKU unless the true-up is in conjunction with their renewal and the customer will be renewing for less users. Instead, you should add users using an appropriately priced subscription product SKU. If you are quoting true-up users, click “Add Products” and wait for the next page to load. Then, click “Select” and choose “Add Add-On Products.” Select True-Up and click Next. Edit the quantity and effective price. Click “Submit.”
*   **Order Form Generation**
    *   If no discounts or special terms are requested, click “Generate PDF.”
    *   If the deal contains discounts that require approval, please submit the quote for approval using the button on the quote. The quote must be approved before the PDF can be generated.
        *   In the case of reseller deals, please obtain the approvals in Chatter based on the approval matrix.
    *   **Order Form Manual Edits**
        *   Please note that Sales reps may only generate PDF versions of the Order Form. If you require special wording or other manual edits, please make the request by tagging @Sales-Support in chatter.


##### Explanation to the prorata calculation of add-on order forms: - [Explanation with screenshot of example order form](https://docs.google.com/presentation/d/1HV4jYVP4yITm0FyhGbDPGmb6f-ebI0Fh480cLfw7u9M/edit#slide=id.ga42daef6d7_0_0)

    *   On add-on/amendment order forms the amounts are prorated based on calendar days as per the following example (adding 10 users to existing subscription)
    *   Annual fee for 10 add-on users: 10 users x $228 = $2,280.00
    *   Number of calendar days of the add-on period: 177 days (7 Apr 2022 - 1 Oct 2022 in this example)
    *   Prorated fee calculation: ($2,280.00 / 365 days) x 177 days = $1,105.64
     



#### **B.  Upgrade or Switch Products During the Subscription Term**
*   Create an “Amend Subscription” quote by following the steps in Section 2 (A) above.
    *   The **Start Date** should be the date of the product exchange. 
    *   On the **Edit Products and Charges** page, select “Add Products” and wait for the next page to load.
    *   Click “Select” and click “Add Base Products.”
    *   Select the **new** product type, and the correct SKU. Click Next.
    *   Select the **Remove** drop down button (which is not fully visible) next to the current product, which you are removing in lieu of the new product.
    *   Adjust the new product line - quantity, discount. Click “Submit.”
    *   Note: On the Order Form, the product being removed will display with a negative amount reflecting the credit for that product for the remainder of the subscription term.


**To supplement these steps, review [THIS TRAINING VIDEO](https://youtu.be/iTVbggacglo) to see each step live for an Amendment Quote:**

**Note:** If you cannot view the video, make sure you are logged in to GitLab Unfiltered. You can [learn more about how to log in or request access to GitLab Unfiltered here!](/handbook/marketing/marketing-operations/youtube/#unable-to-view-a-video-on-youtube) 


#### Renew Subscription Quote

This quote type should be used when the customer has reached the end of their subscription term and wishes to renew the subscription for another term, using the same term start date.  

Note: "Renew Subscription" quotes should be used even if the customer is renewing for a different term length than the previous period. i.e. If the customer is renewing a 12 month subscription for 24 months, choose "Renew Subscription" and enter "24" in the Renewal Term field.

Important: Gaps are not allowed between opps linked to the same subscription ie. new subscription quote object cannot be used on a renewal opportunity, unless you encounter one of the following scenarios. 

2 scenarios when new subscription quote is allowed on a renewal opp:
 - Contract reset (aka early renewal)
 - Customer has lost access or customer decided to renew after 45 days (this is the only scenario where a gap is allowed between renewals)


**A.  Standard Renewal**
*   Open the Renewal opportunity and click the **“New Quote”** button.
*   Select the **existing billing account.**
*   When prompted **select “Renew Existing Subscription for this Billing account,** and select the appropriate subscription. 
    *   Note: If there are multiple billing accounts or subscriptions available, and you are unable to determine which to choose, please ask @Sales-Support via Chatter.
*   Provide Quote, Account, and Subscription Term Details and click “Next.”
*   Select a Quote Template

| Template                            | Use For                                                                                                  |
|-------------------------------------|----------------------------------------------------------------------------------------------------------|
| Standard Order Form                 | Most quotes, including alliance marketplace transactions, or Customers with an Existing Agreement (MSA) in place |
| Standard Order Form (Hide Discount) | Hide the Discount Column for Direct Deals                                                                |
| Authorized Reseller Order Form      | Authorized Reseller Transactions                                                                         |
| MSP Order Form                      | Managed Service Provider Transactions                                                                    |
| Distributor Order Form              | Distributor Transactions                                                                                 |

*   If there is a **signed MSA** between the customer and GitLab, the **“Existing GitLab Agreement Effective Date” field** should be populated with the date of the MSA’s signature. If "Existing Agreement Effective Date" is populated on the quote object, the Order form will automatically generate with Acceptance Language that References the existing agreement. If it **is not populated** the language will default to Standard terms. 

*   Add any preapproved legal language selections to the order form. Checking the box next to the language selection will automatically generate an order form with pre approved legal language - no need for Deal Desk intervention. Selections are as follows:

| Toggle Field                   | Output                                                                                   |
|--------------------------------|------------------------------------------------------------------------------------------|
| Annual Payments                | Annual Payment Language will populate in Payment Details on the Order Form PDF           |
| Customer Reference Language    | Customer Reference Language will populate in Notes Section of the order form             |
| Add Quarterly True Up Language | Standard Quarterly True Up language will populate in the Notes section of the Order Form |
| Remove Signature Block         | Signature Block will be removed. Use for customers with Existing Agreements (MSA)        |

*   The **Start Date** cannot be edited. This will be the true renewal date.
    *   The **End Date** will automatically be determined by the Renewal Term.
    *   Populate **“Renewal Term”** in months. (i.e. for a two-year renewal, enter “24”)
    *   Select the proper **GitLab Entity.**
    *   For **Channel deals**, populate the “Invoice Owner”, “Invoice Owner Contact” and "Resale Partner" fields. For **Direct Deals** leave "Invoice Owner", "Invoice Owner Contact" and "Resale Partner" blank. 
        *   Ensure that the Invoice Owner Contact is connected to the correct reseller account - i.e. if the reseller is in Germany, the Invoice Owner Contact should also be based in Germany. The Billing Account on the Partner Account lists the contact associated with that account, so that contact should be used as the Invoice Owner Contact. If that contact does not exist on the Partner Account as a Contact Record yet, you may need to create it.
        *   If an "Invoice Owner" does not auto-populate in the drop down, this means that a Billing Account does not yet exist for the partner. Chatter `@Billing-ops` and the Partner Account Owner listed on the Partner Account Record and ask for a Billing Account to be created. 
        *   The "Resale Partner" field should be populated with the Partner Account of the Reseller transacting (please note a partner may have both a Customer and Partner Account Record, the Partner Account must be selected. Check the “Account Record Type” field on an account to identify whether it is Customer or Partner account.).
    *   If the customer or reseller is based in the EU, enter the **“VAT ID”** number. This is required to book the order.
    *   If there is a **signed MSA** between the customer and GitLab, the **“Existing GitLab Agreement Effective Date”** field should be populated with the date of the MSA’s signature. In addition, select the **“EXISTING MSA” quote template.**
    *   Note: Non-standard payment terms (Net 30 = standard) should be reflected on the quote object, and will require approval per the approval matrix.
*   Click Next, to enter the Products and Charges page.
    *   Note: The product lines from the Initial Term are already listed and will be marked “Original.” You may add new products, update the quantity on the original license, or remove the existing license. To add users to an existing license at a different price, the new user licenses should be added on a new product line.
    *   For **Channel renewals**, please refer to this [cheat sheet](https://docs.google.com/document/d/1qiT_2EsnL20c4w0hyZ_CGaJQIzj8CSCsHERoR80cwws/edit#heading=h.9e3o7yaxw8mu) to help identify what discounts and product lines to add for a renewal. Quote approvals may be triggered due to system limitations. 
        *   Partner discounts must be added in addition to any customer discounts and required approvals must be received by submitting the quote in the system, per the quote approval matrix. 
        *   If you have questions about Channel discounts when building a quote, chatter `@Partner Operations` for help.


**B.   **Add Annual Payments** to a order form by:
* Checking "Annual Payments" checkbox on Quote Detail Page
* Updating "Initial Term" to 24, 36, 48, or 60. **If Intital Term is not updated correctly, the quote will not generate the correct Total**"
* **All Annual Payment quotes** must use the 1 - Year Product SKU. You will see an error message if you select Annual Payments and anything other than the 1 - Year SKU. 
**C.  SuperSonics: Review the Zuora fields to determine whether any Cloud Licensing features apply to the subscription. For more information on Cloud Licensing, see: [SuperSonics and Sales Assisted Transactions](https://about.gitlab.com/handbook/sales/field-operations/order-processing/#supersonics-and-sales-assisted-transactions) 

**D.  Renewal Using a New Subscription Quote**

If the customer needs to change the Quote Start Date (typically for Billing purposes) at time of Renewal, you will need to build a  New Subscription quote. We will also use a New Subscription quote if the deal includes a Contract Reset. 

*   Open the Renewal opportunity and click the **“New Quote”** button.
*   Select the **existing billing account.**
*   When prompted **select “New Subscription for this Billing account,”** and select the appropriate subscription. 
    *   Note: If there are multiple billing accounts or subscriptions available, and you are unable to determine which to choose, please ask @Sales-Support via Chatter.
*   Fill out the necessary information on the quote per the instruction under the **New Subscription Quote** section above. 
    *   **Note: The Start Date can be later than the original renewal date only if customer has lost access after the renewal grace period. Otherwise no subscription gap is allowed.** 
*   Select a Quote Template

| Template                            | Use For                                                                                                  |
|-------------------------------------|----------------------------------------------------------------------------------------------------------|
| Standard Order Form                 | Most quotes, including alliance marketplace transactions, or Customers with an Existing Agreement (MSA) in place |
| Standard Order Form (Hide Discount) | Hide the Discount Column for Direct Deals                                                                |
| Authorized Reseller Order Form      | Authorized Reseller Transactions                                                                         |
| MSP Order Form                      | Managed Service Provider Transactions                                                                    |
| Distributor Order Form              | Distributor Transactions                                                                                 |

*   If there is a **signed MSA** between the customer and GitLab, the **“Existing GitLab Agreement Effective Date” field** should be populated with the date of the MSA’s signature. If "Existing Agreement Effective Date" is populated on the quote object, the Order form will automatically generate with Acceptance Language that References the existing agreement. If it **is not populated** the language will default to Standard terms. 

*   Add any preapproved legal language selections to the order form. Checking the box next ot the language selection will automatically generate an order form with pre approved legal language - no need for Deal Desk intervention. Selections are as follows: 

| Toggle Field                   | Output                                                                                   |
|--------------------------------|------------------------------------------------------------------------------------------|
| Annual Payments                | Annual Payment Language will populate in Payment Details on the Order Form PDF           |
| Customer Reference Language    | Customer Reference Language will populate in Notes Section of the order form             |
| Add Quarterly True Up Language | Standard Quarterly True Up language will populate in the Notes section of the Order Form |
| Remove Signature Block         | Signature Block will be removed. Use for customers with Existing Agreements (MSA)        |

*   Click Next and update the products and fees per the steps above.

**E.   **Add Annual Payments** to a order form by:
* Checking "Annual Payments" checkbox on Quote Detail Page
* Updating "Initial Term" to 24, 36, 48, or 60. **If Intital Term is not updated correctly, the quote will not generate the correct Total**"
* **All Annual Payment quotes** must use the 1 - Year Product SKU. You will see an error message if you select Annual Payments and anything other than the 1 - Year SKU.  
**F.  SuperSonics: Review the Zuora fields to determine whether any Cloud Licensing features apply to the subscription. For more information on Cloud Licensing, see: [SuperSonics and Sales Assisted Transactions](https://about.gitlab.com/handbook/sales/field-operations/order-processing/#supersonics-and-sales-assisted-transactions) 

**To supplement these steps, review [THIS TRAINING VIDEO](https://youtu.be/kDZJW-ss5j4) to see each step live for an Renewal Quote:**

**Note:** If you cannot view the video, make sure you are logged in to GitLab Unfiltered. You can [learn more about how to log in or request access to GitLab Unfiltered here!](/handbook/marketing/marketing-operations/youtube/#unable-to-view-a-video-on-youtube) 

#### Quoting Annual True-Ups

In accordance with Section 6 of the [GitLab Subscription Agreement](https://about.gitlab.com/handbook/legal/subscription-agreement/#6-payment-of-fees), customers may be required to pay for Overage Users upon the expiration of a Subscription Term in order to renew the Software. 

##### New Annual True-Up SKUs as of 2021-11-15

Effective 2021-11-15, the "Trueup" SKU has been deprecated and replaced with 6 new annual True-Up SKUs, each of which ties to the related subscription SKU. When creating quotes that include a backward looking, one-time True-Up charge, please select the appropriate True-Up SKU as listed below. **For the avoidance of doubt, you must select the True-Up SKU related to the subscription SKU that was in use when the True-Up was incurred.**

* Example 1: A customer has a 100 user SaaS - Premium subscription. At renewal, you learn that the customer incurred 5 Overage Users during the initial subscription term. On the renewal quote, you will select True-Up (Annually) - SaaS - Premium SKU with a quantity of 5.

* Example 2: A customer has a 500 user Self-Managed - Premium subscription. At renewal, they are upgrading to Self-Managed Ultimate for 600 users. During the renewal conversation, you learn that the customer incurred 50 Overage Users during the initial subscription term. On the renewal quote, you will select the True-Up (Annually) - Self-Managed - Premium SKU with a quantity of 50. You will also add the Self-Managed Ultimate SKU for 600 users, and then remove the Self-Managed - Premium SKU.

**Self-Managed True-Up SKUs**

* True-Up (Annually) - Self-Managed - Ultimate: $1,188/seat/year
* True-Up (Annually) - Self-Managed - Premium: $228/seat/year
* True-Up (Annually) - Starter: $48/seat/year

**SaaS True-Up SKUs**

* True-Up (Annually) - SaaS - Ultimate: $1,188/seat/year
* True-Up (Annually) - SaaS - Premium: $228/seat/year
* True-Up (Annually) - Bronze: $48/seat/year

##### New True-Up Selection Flow in Zuora CPQ as of 2021-11-15

To streamline the quoting process, we have created a new product selection step, called "True Ups." When selecting SKUs to add products to a quote, the new True-Up SKUs will display under the third and final page. Previously the "Trueup" SKU was displayed under "Add Add On Products."

![Guided Selling](/handbook/sales/images/trueupsguidedselling.png)

On this new Guided Selling "True Ups" screen, you must select the appropriate True-Up SKU, as displayed below:

![Product Selection](/handbook/sales/images/trueupsproductselection.png)

#### Co-Terming

There are 2 types of co-terming:
1. **Automatic/Same Subscription:** Creating a co-termed Amendment quote to an existing subscription. The Amendment quote will have the same start date as the related existing subscription by default.
2.  **Manual/Separate Subscriptions:** Customer needs a separate new subscription with the same end date as their existing subscription. In order to create a new co-termed subscription you will need to use the monthly sku and also set the start date of the new subscription quote in line with the existing subscription (currently there is no option to create new subscription for a calendar day based prorated period but month based only). For example if the existing subscription runs from 15 Feb 2022 to 15 Feb 2023 and they need another subscription with the same end date but as of April, you would create a new subscription quote with 15 Apr 2022 start date, set the initial term to 10 months and select the monthly product instead of the standard annual one. When creating a new subscription for period < 12 months in order to co-term it with an existing, no approvals are need for a shorter subscription term as per the approval matrix.


#### Quoting Guide: Starter/Bronze End of Availability + Tier Re-naming

**As of 2021-01-26, the following changes have been made to the quoting process in relation to the end of availability of Starter/Bronze:**

* Starter/Bronze SKUs will no longer be selectable for new subscription quotes. 
* Starter/Bronze new subscription deals quoted before 2021-01-26 will be honored and booked through and until the Quote Expiration Date provided that they have received CRO approval.
* Starter/Bronze SKUs will still be selectable for amendment and renewal quotes. Existing Starter/Bronze subscriptions can be amended to add users at the current list price, or renewed for one year at the current list price, until 2022-01-26. Any discounts applied to Starter/Bronze Add-Ons or Renewals will be subject to the [standard discount approval matrix](https://docs.google.com/document/d/1-CH-uH_zr0qaVaV1QbmVZ1rF669DsaUeq9w-q1QiKPE/edit?ts=5d6ea430#).

**Silver/Premium + Gold/Ultimate Legacy SKU Quoting Guide**

As of 2021-01-26, the following changes have been made to the quoting process in relation to the renaming of Silver and Gold:

Salesforce Guided Selling filters have been updated. Legacy SKUs have been removed where appropriate, resulting in a shorter, cleaner list of products that can be sold. Legacy SKUs from previous product deprecations have also been removed.

* All legacy Premium, Silver, Gold, and Ultimate SKUs will begin the deprecation process. **New SKUs have been created in place of these legacy SKUs:**

| New Rate Plan Name      | Legacy Name |
|-------------------------|-------------|
| SaaS - Premium          | Silver      |
| Self-Managed - Premium  | Premium     |
| SaaS - Ultimate         | Gold        |
| Self Managed - Ultimate | Ultimate    |

* **New Business:** New Subscription quotes can only be created using the new SKUs listed above. 
* **Add-Ons:** For existing subscriptions that contain legacy SKUs, all Amend Subscription quotes will use the existing legacy SKU. i.e. If you upsell an existing Gold subscription, your amendment quote and Order Form will still use the legacy Gold SKU.
* **Renewals:** For existing subscriptions that contain legacy SKUs, **all Renew Subscription quotes will require that the legacy SKU be removed from the quote, and that the new SKU be added to the quote in its place for the renewal.** When quoting a renewal, *please follow the steps below to change the SKU:*
  * First, click **Select Products**
  * On the **Edit Products and Charges** page, select "Add Products"
  * Click "Select" and then click "Add Base Products"
  * Select the new SKU. Click Next.
  * Select the **Remove** drop down button next to the legacy SKU.
  * On the new SKU, adjust the quantity and pricing as needed. Click "Submit."
  * Note: On the Order Form, the legacy SKU will appear as "Removed."

* **Legacy SKU Grace Period**: Any quotes/Order Forms created before 2021-01-26, on which a legacy SKU was used, will be accepted for booking. All quotes/Order Forms created on or after 2021-01-26 must follow the applicable quoting requirements for each quote type listed above.

**Quote Discount Approval Module Updates**

* The quote approval module has been updated to recognize the new Premium and Ultimate SKUs referenced in the table above.
* The quote approval module has been updated to recognize the [Starter/Bronze EoA Option 2 upgrade offer](https://docs.google.com/document/d/19T-ysFuEFWIlAv7Z9o1Q6-kVU9HKFb_nzGZ1uumpMLA/edit) (one year renewal/add-on upgrade).

#### SuperSonics Billing and Subscription Management Experience

To learn more about the SuperSonics Billing and Subscription Management experience and how it impacts your quote, review the following:
- [SuperSonics and Sales Assisted Transactions](https://about.gitlab.com/handbook/sales/field-operations/order-processing/#supersonics-and-sales-assisted-transactions)
- [How To Opt-Out of SuperSonics Features](https://about.gitlab.com/handbook/sales/field-operations/order-processing/#how-to-opt-out-of-supersonics-features) 
- [How to Temporarily Pause SuperSonics Features](https://about.gitlab.com/handbook/sales/field-operations/order-processing/#how-to-temporarily-pause-supersonics-features)

#### Quoting Professional Services

**There are two separate ways to quote Professional Services, depending on whether the services are standard, or scoped/custom.**

##### Creating a Professional Services Quote for Standard Services

*   Standard services are those for which a SKU exists in Zuora CPQ.
*   Standard services can be added to any New Business, Add-On, or Renewal quote alongside subscription product SKUs. (i.e. If you are selling a 12 month SaaS Ultimate deal, you can add a separate SKU on the same quote to sell the Rapid Results offering.)
*   For Standard Services, a signed Order Form is sufficient to book the deal. (Note, some customers may also require issuance of a PO)
*   More information on [Professional Services SKUs](/handbook/customer-success/professional-services-engineering/#professional-services-skus) 

##### Before submitting a Scoped/Custom Professional Services Opportunity for Closure:

*   For [standard professional services skus](/handbook/customer-success/professional-services-engineering/#current-skus), only a signed order form is required.

##### Creating a Professional Services Quote for Scoped/Custom Services

*   Scoped/Custom Services are those that require consultation with the Professional Services team, and require a custom SOW to be drafted and signed by the customer. This includes any Professional Services that are not fully prepaid and any cases where a combination of standard sku offerings and custom-scoped services are being sold together in a single opportunity.
*   Scoped/Custom Services must always be quoted on a standalone opportunity, separately from any subscription products SKUs. (i.e. You may NOT sell a 12 month SaaS Ultimate SKU on the same quote/opp as Scoped/Custom Services).
*   Create a New Business Opportunity and select “Professional Services Only” under “Opportunity Record Type.”
*   Create a new subscription quote under the Professional Services opportunity by following the steps above under “new Subscription Quote.” Select the **GitLab Service Package** SKU. Update the price to reflect the total price on the SOW.  You should use this sku to reflect the total amount of the SOW, even if the SOW includes some standard training classes or other offerings that could be purchased via sku.  **Note:** You do not need to submit the quote for Custom services for approval. Approvals will be handled by the Engagement Manager via the scoping issue, and any discounts in rate should be handled separately via Chatter, as per the [Approval Matrix](https://docs.google.com/document/d/1-CH-uH_zr0qaVaV1QbmVZ1rF669DsaUeq9w-q1QiKPE/edit#heading=h.dccvx02huo2y).  The SOW will be sent to the customer for signature, not the quote/order form.
*   To gain support from the Professional Services team with a Custom SOW, initiate a scoping issue using the [Services Calculator](https://services-calculator.gitlab.io/) and work with a Professional Services [Engagement Manager](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/engagement-mgmt/).

##### Before submitting a Scoped/Custom Professional Services Opportunity for Closure:

* Ensure that a supporting Quote object has been created with the amount matching the SOW amount
* Please note that the following items must be attached to the opportunity before it can be Closed Won:
    *   SOW signed by **both the customer and GitLab's PAO or CFO.****
    *   Cost Estimate (COGS) spreadsheet (Link provided by the PS team)

#### How to Clone an Existing Quote

If you'd like to save time by cloning an existing quote, you can do so by doing the following:

1. On the Quote Detail page of the quote you want to clone, click Clone Quote.
1. On the Quote Clone Configuration page, select the following options:
    * Clone Products: Select to clone the products associated with the quote. This option only applies to the New Subscription quotes. Ensure that the products associated with the quote are maintained in the current version of the product catalog.
    * Maintain Quote: Select to be directed to the first step in the quote object workflow that allows you to edit the newly cloned quote. The flows are configured based on the quote type, i.e., New Subscription, Amendment, and Renewal, in the quote object workflow.
1. Click Next.
    * If you selected the Maintain Quote option, you are redirected to the first step in the Quote Wizard of the newly cloned quote.
    * If you did not select the Maintain Quote option, you are redirected to the Quote Detail page of the newly cloned quote.
1. Please note that you cannot clone the products on an amendment (add-on or renewal quote.)

#### How to Create a Draft Proposal

Follow the standard process for [quote creation](https://about.gitlab.com/handbook/sales/field-operations/sales-operations/deal-desk/#zuora-quote-configuration-guide---standard-quotes). The Quote Object **does not** need to be approved before generating a Draft proposal. 

1. Click Edit Quote. 
2. Select the Draft Quote Template. Save. 
3. Click Generate PDF. A Draft Proposal PDF will be attached to the opportunity in the Notes & Attachments section. 

**Important Notes** 
- A Draft Proposal PDF is not an Order Form. All quotes must go through the applicable approval process before you can generate an Order Form. Draft Proposals are not guaranteed approval.
- A Draft Proposal PDF will not be accepted in place of an Order Form under any circumstance. 
. To generate a legitimate order form, you must update the Quote Template from the Draft selection to relevant order form template for the opp

### **Non-Standard Quotes**

Occasionally an opportunity will require a unique structure that is outside of the normal quote format. Examples of these scenarios are listed below. Deal Desk will partner with the Account Owner to structure the opportunity and provide guidance on creating the quote. Please chatter on the opportunity if you need assistance with one of these scenarios! 

#### Contract Reset

Contract Resets are used to perform an "Early Renewal" - i.e. start a new 12 month subscription before the renewal date. They can also be used if a customer needs to change a billing date or would like to change the term length, mid term. 

*   Open the Renewal opportunity and click the **“New Quote”** button.
*   Select **existing billing account.**
*   When prompted **select “New Subscription for this Billing account,”** and select the appropriate subscription. 
    *   Note: If there are multiple billing accounts or subscriptions available, and you are unable to determine which to choose, please ask @Sales-Support via Chatter.
*   Fill out the necessary information on the quote per the instruction under the New Subscription Quote section above. 
    *   **Note: The Start Date should be the new subscription term’s start date, or the “Early Renewal” date.**
*   Click Next and update the products and fees per the steps above.
*   **IMPORTANT:** Next, tag `@Sales-Support` in Chatter on the Renewal Opportunity to create a credit opportunity and quote to cancel the existing subscription, which in this scenario is being replaced with the new subscription. Deal Desk will then manually generate a Contract Reset Opp and Order Form to add the credit line into the order form once the quote has been fully approved.
*   Deal Desk will attach a PDF of the consolidated quote to the opportunity 
*   Sales team will send the order form to the customer for signature
*   Upload the signed order form to the renewal opp and to the credit opp as well and submit both opps for approval

On the main contract reset opportunity, populate the "Opportunity Category" field with "Contract Reset." On the associated credit opportunity, populate the "Opportunity Category" field with "Credit." 

All Contract Reset opportunities will be classified as a "Renewal" and will be subect to ARR Basis, which will impact overall Net ARR depending on the scenario. 

For more information on ARR calculation for Contract Resets, see [ARR in Practice](https://about.gitlab.com/handbook/sales/sales-term-glossary/arr-in-practice/#calculating-net-arr-for-contract-resets).


#### Concurrent Subscriptions 

Concurrent Subscriptions are two unique subscriptions (instances) for Self Managed and SaaS issued for the purpose of supporting a large customer migration over an extended period. **This does not allow the customer to use Self Managed and SaaS on the same instance.** 

Approvals for this deal strucutre will follow the [standard approval matrix](https://docs.google.com/document/d/1-CH-uH_zr0qaVaV1QbmVZ1rF669DsaUeq9w-q1QiKPE/edit#heading=h.f8920bj8v6l9).

** Deal Structure Requirements** 
- The Start Date and End Date must align on both subscriptions, if the Self Managed and SaaS instance are created / booked at the same time. 
- If the customer is adding a migration mid term, the End Date of the added subscription must align with the existing subscription. (Ex. Self Managed 1 Year. The customer adds a SaaS instance mid way through the term to support migration efforts. The start date of the Self Managed Subscription is 01-01-2000 to 12-31-2000. The SaaS instance term dates should be 06-01-2000 to 12-31-2000.)
- SKU type is subject to our standard bookign requirements - SKU term must match the Initial Term, unless the deal includes Annual Payments. 
- Custom Legal Langauge is required on all order forms dictating the terms and purpose of the migration. 
- The Sold To and Bill To Contact for Self Managed and SaaS can be the same person. The subscriptions can be attached to the same billing account. 
- Each subscription (Self Managed / SaaS) will be booked on separate opportunities / separate quotes within SFDC. A consolidated order form will be provided to the customer for convieninece. 

Deal Desk will work closely with Sales, Legal, and Revenue to ensure all booking requirements are met. Please work directly with Deal Desk to structure this deal accurately. Please tag sales support early in the deal negotiation. 

#### Multi-Year Deals

In the case of multi-year deals, the Initial Term should be updated to reflect the number of months in the subscription term - 24, 36, 48, 60, etc. 

For prepaid multi-year deals, you must select the correct multi-year SKU (i.e. for a prepaid 2 year SaaS - Premium deal, select “SaaS - Premium - 2 Year”). Prepaid multi-year deals that do not utilize the correct multi-year SKU will be rejected. 

For multi-year deals, paid annually, you must select the 1 Year SKU, and click on the Annual Payments checkbox to add the necessary payment language to the Order Form. Multi-year deals paid annually that do not utilize the correct 1 Year SKU will be rejected.



For more information on ARR calculation for Multi-Year Deals, see [ARR in Practice](https://about.gitlab.com/handbook/sales/sales-term-glossary/arr-in-practice/#how-net-arr-is-calculated-for-standard-deals).

### Ramp Deals 

A ramp deal is when a contracts a multi-year deal for users that will be fulfilled on a set annual schedule throughout the duration of the multi-year subscription term. In a Ramp, users are prorated from when they will be added. The customer pays upfront, or annually, for all users planned in the ramp schedule. For [Amendment quotes](/handbook/sales/field-operations/sales-operations/deal-desk/#amend-subscription-quote), the customer adds users throughout the subscription with no set schedule. 

Ramp deals are limited to multi-year deals, with 12 month ramp periods. See [ARR in Practice](https://about.gitlab.com/handbook/sales/sales-term-glossary/arr-in-practice/#calculating-net-arr-for-ramp-deals) for more information.


#### How to Create a Ramp Deal

A.  To create a ramp deal, tag @Sales-Support in chatter on the opportunity. **Provide the following information for each ramp period:**
*   Start date/term length
*   Proposed ramp schedule
*   Bill To and Sold To Contact
*   Invoice Owner and Invoice Owner Contact (If Partner Deal)
*   Product, quantity, discount
*   Payment Terms (i.e. Net 30)
*   Any other requests (i.e. Price Lock, true up language, etc.)

B.  Request template:
*   Deal Desk has created a [G Sheet template](https://docs.google.com/spreadsheets/d/1ho_ndKIZDvgdWOn873XONK2oc1tLloVJwlnpxgHYjiE/edit#gid=0) for Sales reps to enter ramp information.
    *   To use this template, copy the template to your own Drive, enter the information, and share your G Sheet with @Sales-Support in Chatter.

C.  Deal Desk will create the quotes and Order Form.

On the opportunity, populate the "Opportunity Category" field with "Ramp."

For more information on ARR calculation for Ramp Deals, see [ARR in Practice](https://about.gitlab.com/handbook/sales/sales-term-glossary/arr-in-practice/#calculating-net-arr-for-ramp-deals).

#### How to Process a Professional Services SOW Amendment (Change Order)

Occasionally, changes will be made to a Custom SOW after an opportunity has been booked. These changes could include delivery (invoice) schedules or types of services delivered. The total value of the opportunity **should not change.** If the total value of an opportunity is negatively impacted, this will require an internal refund and rebooking of the order. Should there be additional revenue generated as a result of this Change Order, a new Professional Services Only opportunity is required to account for the _delta value_.

#### Creating Decomission Opportunties

Deal Desk will create Decommission (refund) opportunities for all Sales Assisted Orders. The Billing team will create Decommission (refund) opportunities for all Web Portal purchases. The creation process is the same for both Deal Desk and Billing. 

To create the decommission opportunity, navigate to the related Closed-Won Opportunity. Click 'Create Refund Opportunity'. Navigate to the opportunities tab under the Account page. Click on the newly created decommission opp, it will have 'REFUND' in the opportuntiy name. On the opportunity level, double check all fields populated as expected. For some multi-year deals, or partial refunds, 'Amount' may need to be updated manually. 

**Deal Desk** - After confirming all fields are correct on a Sales Assisted decommission, submit the opp for approval. 
**Billing** - After confirming all the fields are correct on a Web Portal Purchase, manually change the 'Stage' to 'Closed-Won'. 

#### Payment Schedule

*  Clone the original Closed-Won opportunity. Update the opportunity name to include “Amended SOW”.
*  Update the Original Closed-Won opportunity as duplicate, linking to the Amended SOW Opportunity.
*  Build a quote identical to the original closed-won opportunity. Update the quote to primary.
*  Attach the original SOW, the Amended SOW, and link to Cost Estimate to the Amendment Opportunity.
*  Submit the Opportunity for Deal Desk/Billing Approval.
*  The Opportunity Close Date for the Amended SOW should be manually updated to match the original Closed-Won opportunity.

#### Opportunities Requiring Multiple Invoices

If an opportunity requires multiple invoices due to a specific professional services delivery schedule, a separate opportunity is required for each invoice period. If there is no difference in number of seats or price across the years only one subscription and quote would be required (ie. Invoice Only opps do not require a quote object).

In FY22, multi year deals for recurring subscription products, paid annually, will be captured on one opportunity. 

**Opportunity Structure**

* Invoice Only Opp type should be New Business
* Each individual opportunity will require a quote object that is equal to the amount being invoiced
* All products, dates, and contacts should match the original opp / quote
* Build Invoice Only quote objects as a "New Subscription" quotes

**Invoice Amounts** 

If all payments associated with the opportunity are equal (ex. 3 payments of $10,000) the quote on the original opporunity must reflect the entire opportunity term. 

**Ex.** 

3 Year Subscription worth $30,000, broken out into 3 equal annual payments of $10,000. 
* Primary Opp Quote - 3 Year New Subscription Quote (using the 1 Year Product SKU). Term Length should be 36. 
* Invoice Only Opp - Year 2 - Update the Amount field to reflect the total to be invoiced. A quote object is not required.
* Invoice Only Opp - Year 3 - Update the Amount field to reflect the total to be invoiced. A quote object is not required.

If the payment amounts or user count per year are not identical, (ex. Year 1 - $15,000 Year 2 - $10,000, Year 3 $5,000)- then the original opporunity quote should only reflect the first year (invoice period) of the subscription. 

**Ex.** 

3 Year Subscription worth $30,000, broken out into 3 payments. Year 1 - $10,000, Year 2 - $7,000, Year 3 $13,000
* Primary Opp Quote - 3 Year New Subscription Quote (using the 1 Year Product SKU). Term Length should be 36. 
* Invoice Only Opp - Year 2 - Update the Amount field to reflect the total to be invoiced. A quote object is not required. (Use the same SKU as the original opp, update initial term to 12)
* Invoice Only Opp - Year 3 - Update the Amount field to reflect the total to be invoiced. A quote object should be created to reflect the total amount to be invoiced. (Use the same SKU as the original opp, update initial term to 12)

**Ex.** Professional Services Deal -Opportunity amount $300,000. 4 Deliverables are outlined in the Custom SOW to the customer, each deliverable includes a different date for delivery. This requires 4 opportunities because the customer will be invoiced after completion of each deliverable.


*  Opp 1 (Original Opportunity) - First Deliverable - $50k in services
*  Opp 2 (Cloned from Original) - Second Deliverable -$50k in services
*  Opp 3 (Cloned from Original) - Third Deliverable - $100k in services
*  Opp 4 (Clone from Original) - Fourth Deliverable - $100k in services

To create opportunities requiring multiple invoices: 

*  Clone the original forecasted opportunity
*  Update the opportunity name to include “Deliverable # and the Date of delivery. The close date should be the same for all opportunities (if SOW and Cost Estimate was received on 1/1/2020, all opportunities should show 1/1/2020 as the close date even if it will be invoiced in a future period).
*  Create a quote reflecting the value of the Deliverable. Build out each quote using the same SKU and discount (if applicable). The start date should match the delivery date outlined in the SOW.
*  The quote should follow the standard quote creation process (link to quote building process). Set the quote as primary. Repeat for all opportunities. The sum amount of all opportunities should equal the total amount for the SOW.
*  Attach the signed SOW and Cost Estimate link to **each opportunity.**
*  Submit the Opportunity for Deal Desk/Billing approval. The Billing team will flag each deliverable opportunity for future invoice periods.

Note: The "Payment Schedule" field on each opportunity should be populated with the value "Annual Payments" or "Custom Payment Schedule," whichever applies.

**Add-Ons to Opportunities with Multiple Billing Events**

- Scenario: The customer has a two-year subscription, paid annually. Each year's invoice amount is $100,000. 6 months into the subscription, the customer would like to add 100 users, totalling $75,000 over the remaining 18 months of the agreement.
  - In scenarios where we are amending a subscription where there are one or more outstanding out-year payments, one quote should be provided, where the base product is amended as appropriate.
  - How to read the Quote Subtotal: In these scenarios, the quote's subtotal will be higher than anticipated, as the subtotal will account for (1) the add-on you're currently quoting, as well as (2) the year 2 payment (which is scheduled and hasn't been invoiced yet).
  - How to prepare the Order Form: In these scenarios, Deal Desk will manually prepare the Order Form by (1) Generating the Order Form, (2) subtracting the oustanding year 2 payment from the Order Form Grand Total, and (3) adding annual payment language and totals to the Order Form. 
  - How to prepare the Opportunities: In these scenarios, the primary opportunity will represent the remainder of the year in question. For each year remaining in the agreement, Deal Desk should (1) create a debook opportunity to debook the current out-year Invoice opportunity, and (2) create a rebook opportunity to correctly rebook the out-year Invoice opportunity with the new, increased out-year Invoice total (representing the original Invoice amount PLUS the add-on Amount for that future period)
  - In these scenarios
    - Example: From the scenario above, the quote's subtotal would read $275,000 because it includes (1) the $75,000 Add-On, and (2) the outstanding $100,000 Year 2 Invoice/Payment. 
    - Order Form: Deal Desk would (1) Change the Grand Total to $75,000, (2) Add Annual Payment language, and (3) in the table, write that $25,000 is due upon receipt (represents the add-on amount for the remainder of the first year), and that $50,000 is due on the same date as the outstanding Year 2 invoice. 
    - Opportunities: There would be an existing Year 2 Invoice Opportunity with an Amount of $100,000. Deal Desk would debook that opportunity. Deal Desk would then rebook that opportunity with an Amount of $150,000.
- Real-Life Example: https://gitlab.my.salesforce.com/0064M00000YQO1c?fId=0D54M0000465HEc

### Miscellaneous (But Important) Information related to Quotes

A.  **To add users to an existing license at a different price**, please add the users on a new, separate product line.

B.  **To create a true-up/add-on quote for a multi-year deal**, please add **both** the true-up and increase the license count by the same number of users. Note that the user number cannot decrease during the term of a multi-year deal - i.e. in the case of a three-year deal, if the customer exceeds the 100 license count by 25 users, (1) True-Up SKU with 25 users, and (2) increase the license quantity from 100 to 125.

C.  **If the customer signs a renewal quote, but a true-up is required before the renewal date**, create an add-on opportunity from the closed renewal opportunity, use the same start date as the renewal, and add the necessary true-up.

D.  If you have **multiple quote objects** under one opportunity, the quote you are using **must be marked Primary.**

### Alliance Marketplace Private Offers

Effective Friday, 2022-04-08, Deal Desk manages AWS and GCP Private Offer creation and acceptance. 

#### Process Overview 

- **All Private Offer requests must be made in chatter by tagging @Sales-Support on the related SFDC opportunity.** All Private Offer requests must include the following information:
  - Customer's AWS or GCP Billing ID
  - Customer Contact name and email address
    - Note: This contact must have the necessary permissions to accept the Private Offer in the marketplace
- After a Private Offer request is made, Deal Desk will review the request and confirm the quote is tagged appropriately for AWS/GCP and confirm the Order Form acceptance section also includes AWS or GCP marketplace language.
- Deal Desk will then create the private offer and send the offer to the contact identified by the opp owner. Notification emails will be sent to the opp owner to track the offer status.
- At time of acceptance, an email will be sent to the GitLab Team and Deal Desk will chatter the documentation to close the deal.
- The AWS deals do not need to be signed.
- When the deal is Closed Won, the license key file/subscription details will be automatically sent to the 'Sold To' contact on the GitLab Order Form.

#### Working with Deal Desk vs. Alliance Business Development Managers (ABDMs)

Please note that Deal Desk handles only Private Offer creation, edits to Private Offers, and the acceptance of Private Offers. For any other matters related to Alliances deals, **Sales must work with the appropriate ABDM.** The Alliances team can provide assistance and best practices that are vital to a successful marketplace transaction. 

To reach an ABDM, use the #a_aws_deal_registration or #a_gcp_deal_registration Slack channels.

#### Alliance Deal/Private Offer Resources

- Internal Handbook: 
  - [Google Cloud Platform](https://gitlab-com.gitlab.io/alliances/alliances-internal/gcp/)
  - [Amazon Web Services](https://gitlab-com.gitlab.io/alliances/alliances-internal/aws/)
- Marketplace FAQ
  - [Google Cloud Private Offers](https://docs.google.com/document/d/1S0k0T12xkXajpHt9uvl3YDA4OSOwS6qB9pHmRKiqCgA/edit#bookmark=id.5hmmfbjqr9xn)
  - [Amazon Web Services Private Offer](https://docs.google.com/document/d/1S0k0T12xkXajpHt9uvl3YDA4OSOwS6qB9pHmRKiqCgA/edit#bookmark=id.ee7ef9xn6p1k)

### Opportunity Categorization

The following fields are maintained by Deal Desk to distinguish standard vs. non-standard opportunities:

#### Opportunity Category

Opportunity Category Definitions are viewable [here](https://docs.google.com/document/d/1UaKPTQePAU1RxtGSVb-BujdKiPVoepevrRh8q5bvbBg/edit#heading=h.te2fab3byieu).

#### Opportunity to Decommission

*  This is a lookup field, where the name of the original opportunity (now being decommissioned) is entered. This field is required by validation rule if Opportunity Category = Decommission. Upon saving, the linked opportunity will automatically be categorized as "Decommissioned" via process builder.

#### Payment Schedule

*  Prepaid (Default Value)
*  Annual Payments
*  Custom Payment Schedule

#### Quote Entity Information

On Order Forms, GitLab entity information will be populated via the following rules. This table is based on the [ISO-2 billing country code](http://www.nationsonline.org/oneworld/country_code_list.htm) of the direct customer or reseller we are delivering invoices to:

**New Subscription and Renew Subscription Quotes**

| Customer's Billing Country | Partner's Billing Country | GitLab Quote Entity |
| ------ | ------ | ------ |
| NL | NL | BV (NL) |
| DE  | DE | GmbH (DE)|
| UK | UK | Ltd (UK) |
| All countries outside of NL, DE, UK, AU  |All countries outside of NL, DE, UK, AU | Inc. (US) |
| AU  | AU | PTY LTD (AU) |

**Note** For direct deals, Billing Country is based on the mailing address of the Bill To contact listed on the quote object. For partner deals, Billing Country is based on the mailing address of the Invoice Owner Contact listed on the quote object.

**Amend Subscription Quotes**

| Initial Transaction Method | Amendment Transaction Method | GitLab Quote Entity |
| ------ | ------ | ------ |
| Web Direct | Web Direct | Inc. (US) |
| Web Direct   | Sales-Assisted | Inc. (US) |
| Sales-Assisted | Sales-Assisted | Same quote entity as on base subscription`s billing account |
| Sales-Assisted  | Web Direct | Same quote entity as on base subscription`s billing account |

**Note**: All initial web direct subscriptions ordered through the web store are placed on the US entity. For sales-assisted add-ons to subscriptions initially purchased via web store, the amendment quote/Order Form must reflect the US entity.

**Custom Quote Entity Rules**

| SFDC Account Name | Name Of Billing Account(s) | Search Word For Exception Rule In Invoice Owner Field | GitLab Quote Entity |
| ------ | ------ |------ |------ |
| [Amazic EMEA (Parent)](https://gitlab.my.salesforce.com/0016100001ecGXx?srPos=0&srKp=001) | Amazic, Amazic BV, Amazic Germany GmbH, Amazic UK Ltd | "Amazic"| Inc. (US)|
| [Google Cloud (Parter)](https://gitlab.my.salesforce.com/0014M00001nJhks)  | Google Cloud Marketplace | "Google Cloud Marketplace" |Inc. (US) |
| [Amazon Web Services](https://gitlab.my.salesforce.com/0014M00001ldTdt?srPos=1&srKp=001) | Amazon Web Services, Inc. | "Amazon Web Services" |Inc. (US) |
| [Epidata SA](https://gitlab.my.salesforce.com/00161000015Lyf9?srPos=0&srKp=001) | Epidata SA | "Epidata SA" | BV (NL) |

##### How to Update Account IDs in Zuora

Zuora Account information is linked to Salesforce Account Information via `Account IDs`. If this information is not updated, Zuora cannot sync existing billing or subscription information to new quotes. 

1. Go to Zuora, under the Customers tab, search the account name, naviagate to the Account Overview page
1. Ensure that the CRM Account ID shows a hyperlinked account name with a green check mark. 
1. If it shows a Yellow exclamation mark, go to the primary account that is left from the merge. Copy the last 15 characters in the account page URL and paste into the CRM Account ID field in Zuora. Click Save. 

If Zuora successfully maps to the correct account, a green check box will appear.

