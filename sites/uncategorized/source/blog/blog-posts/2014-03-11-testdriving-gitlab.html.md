---
title: Testdriving GitLab
description: "If you want to give Gitlab a quick testdrive, there are several options available for you. We show you the easiest ways in our latest screencast."
canonical_path: "/blog/2014/03/11/testdriving-gitlab/"
date: March 11, 2014
author: Job van der Voort
categories: company
---
If you want to give Gitlab a quick testdrive, there are several options available for you. We show you the easiest ways in our latest screencast:

- Using [demo.gitlab.com](http://demo.gitlab.com)
- Trying out [GitLab Cloud](https://gitlab.com)
- Provisioning a VPS with GitLab preinstalled on [DigitalOcean](https://www.digitalocean.com)

<iframe width="560" height="315" src="//www.youtube.com/embed/WaiL5DGEMR4" frameborder="0" allowfullscreen></iframe>
